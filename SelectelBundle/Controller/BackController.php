<?php
namespace itfind\SelectelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BackController extends Controller{


	private $ch;

	private $url;

	private $uname;

	private $ukey;

	private $headers;

	private $prefix;

	private $token;

    private $expired;


    public function __construct($params){
        $this->uname = $params['corename'];
    }

	private function setToken($token){
		$this->token = $token;
	}

	protected function setHeaders($headers){
		$this->headers = $headers;
	}

	protected function setUrl($url){
		$this->url	= $url;
	}

    /**
    * set $ch
    */
	protected function setCh(){
		if(null !== $this->url)
			$this->ch = curl_init($this->url);
	}

    /**
    * @param $ukey
    */
	protected function setUkey($ukey){
		$this->ukey = $ukey;
	}

    /**
    * @param $prefix
    */
	protected function setPrefix($prefix){
		$this->prefix = $prefix;
	}

    /**
    * @param $expired
    */
    protected function setExipired($expired){
        $this->expired = $expired;
    }
    /**
    * @param $ukey
    * @param null $uprefix
    * @return array
    */
	public function CloudAuthRequest($ukey, $uprefix=null){
		$this->setUrl('https://auth.selcdn.ru/tokens');
		$this->setCh();
		$this->setUkey($ukey);
		if(null !== $uprefix)
			$this->setPrefix($uprefix);
        if(null !== $this->prefix){
            $uprefixname = $this->uname.'_'.$this->prefix;
        }else{
            $uprefixname = $this->uname;
        }

		$auth_data = [
			'auth' => [
				'passwordCredentials' => [
					'username' 	=> $uprefixname,
					'password'	=> $this->ukey,
				],
			]
		];
		$headers = json_encode($auth_data, true);
		$this->setHeaders($headers);
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_HTTPHEADER,['Content-type: application/json']);
		curl_setopt($this->ch, CURLOPT_POST, true);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->headers);
		$response = curl_exec($this->ch);
		curl_close($this->ch);
		$data = json_decode($response, true);
		$this->setToken($data['access']['token']['id']);
		$authdata = [
			'username'  => $this->uname,
			'prefix'	=> $this->prefix,
			'password'  => $this->ukey,
			'token' 	=> $this->token,
			'url'		=> $data['access']['serviceCatalog'][0]['endpoints'][0]['publicURL'],
            'expires'   => $data['access']['token']['expires'],
		];

		return $authdata;
	}

    /**
    * @param $token
    * @param $url
    * @param $prefix
    * @return mixed
    */
    public function getClientFiles($token, $url, $prefix){
        $this->setUrl($url.'/'.$prefix.'/?format=json');
        $this->setCh();
        $this->setHeaders(['X-Auth-Token:'.$token]);
        curl_setopt($this->ch, CURLOPT_HTTPGET, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER,$this->headers);
        $response = curl_exec($this->ch);
        curl_close($this->ch);
        $data = json_decode($response, true);
        return $data;
    }
    /**
    * @param $key
    * @param $token
    * @param $url
    * @param $prefix
    * @param $filename
    * @return mixed
    */
    public function generateLink($key, $token, $url, $prefix, $filename){
        $this->setUrl($url.'links/'.$token.'/'.$filename);
        $this->setCh();
        $hash = sha1($key+'/'.$prefix.'/'.$filename);
        $headers    = [
            'X-Auth-Token:'.$token,
            'Content-Type: x-storage/symlink',
            'X-Object-Meta-Location: /'.$prefix.'/'.$filename,
            'X-Object-Meta-Link-Key:'.$hash,
            'X-Object-Meta-Delete-At:'.strtotime("+30 min"),
            'Content-Length: 0',
        ];
        $this->setHeaders($headers);
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER,$this->headers);
        $response   = curl_exec($this->ch);
        if(null==$response)
            return $this->url;
    }

    /**
     * @return bool
     */
    public function isExpired(){
        $date = new \DateTime('NOW');
        if($this->expired <= $date):
            return true;
        else:
            return false;
        endif;
    }



}