<?php
namespace itfind\SelectelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\MemcacheCache;
use Symfony\Component\HttpFoundation\Session\Session;

class BackCoreController extends Controller{

    protected $ch;
    protected $url;
    protected $uname;
    protected $ukey;
    protected $authost;
    protected $headers;
    protected $token;
    protected $expired;
    protected $session;

    public function __construct($params, Session $session){
        $this->session  = $session;
        $this->uname    = (string)$params['corename'];
        $this->ukey     = $params['corekey'];
        $this->authost  = $params['authost'];
        $this->token    = $session->get('token');
        $this->url      = $session->get('CloudUrl');
        $this->expired  = $session->get('TokenExpired');

    }
    protected function setCh(){
        if(null !== $this->getUrl())
            $this->ch = curl_init($this->url);
    }
    protected function setUrl($url){
        if($url != $this->url || !empty($url))
            $this->session->set('CloudUrl', $url);
            $this->url = $this->session->get('CloudUrl');
    }
    protected function setHeaders($headers){
        $this->headers = $headers;
    }
    protected function setToken($token){
        $this->session->set('token', $token);
        $this->token = $this->session->get('token');

    }
    protected function setExpired($expired){
        $this->session->set('TokenExpired', $expired);
        $this->expired = $this->session->get('TokenExpired');

    }
    public function getExpired(){
        return $this->expired;
    }
    public function getToken(){
        return $this->token;
    }
    public function getUrl(){
        return $this->url;
    }

    public function authCloud(){
        if((null == $this->session->get('token')) || ($this->isExpired($this->session->get('TokenExpired'))) ){
            $this->setUrl($this->authost);
            $this->setCh();
            $auth_data = [
                'auth' => [
                    'passwordCredentials' => [
                        'username' 	=> $this->uname,
                        'password'	=> $this->ukey,
                    ],
                ]
            ];
            $headers = json_encode($auth_data, true);
            $this->setHeaders($headers);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER,['Content-type: application/json']);
            curl_setopt($this->ch, CURLOPT_POST, true);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->headers);
            $response = curl_exec($this->ch);
            curl_close($this->ch);
            $data = json_decode($response, true);
            $this->setToken($data['access']['token']['id']);
            $this->setUrl($data['access']['serviceCatalog'][0]['endpoints'][0]['adminURL']);
            $this->setExpired($data['access']['token']['expires']);

        }
        $authdata = [
            'username'  => $this->uname,
            'password'  => $this->ukey,
            'token' 	=> $this->token,
            'url'		=> $this->url,
            'expires'   => $this->expired,
        ];
        return $authdata;
    }
    public function createContainer(){}

    public function listContainers(){
        $url        = $this->getUrl();
        $queryUrl   = $url.'?format=json';
        $this->setUrl($queryUrl);
        $this->setCh();
        if($this->token == null)
            $this->setToken($this->token);
        $this->setHeaders(['X-Auth-Token: '.$this->token]);
        curl_setopt($this->ch, CURLOPT_HTTPGET, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
        $response = curl_exec($this->ch);
        $data = json_decode($response, true);
        curl_close($this->ch);
        $this->setUrl($url);
        return $data;
    }
    public function listFilesInContainer($cname){
        $url        = $this->url;
        $queryurl   = $this->url.$cname.'?format=json';
        $this->setUrl($queryurl);
        $this->setCh();
        $this->setHeaders(['X-Auth-Token:'.$this->token]);
        curl_setopt($this->ch, CURLOPT_HTTPGET, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
        $response   = curl_exec($this->ch);
        curl_close($this->ch);
        $this->setUrl($url);
        $data       = json_decode($response, true);
        foreach($data as $key=>$val){
            $data[$key]['url'] = $this->url.$cname.'/'.$val['name'];
        }
        return $data;
    }

    public function grantPriviliges(){}

    /**
     * @return bool
     */
    public function isExpired(){
        $date = new \DateTime('NOW');
        if($this->expired >= $date):
            return true;
        else:
            return false;
        endif;
    }



}