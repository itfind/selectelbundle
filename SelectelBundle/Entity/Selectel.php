<?php

namespace itfind\SelectelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;




class Selectel{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="expireDate", type="datetime", nullable=false)
     */
    protected $expireDate;

    /**
     * @var string
     * * @ORM\Column(name="token", type="string", nullable=false)
     */
    protected $token;

    /**
     * @var string
     * @ORM\Column(name="uname", type="string", nullable=false)
     */
    protected $uname;

    /**
     * @var string
     * @ORM\Column(name="prefix", type="string", nullable=false)
     */
    protected $prefix;

    /**
     * @var string
     * @ORM\Column(name="url", type="string", nullable=false)
     */
    protected $url;

    /**
     * @var string
     * @ORM\Column(name="ukey", type="string", nullable=false)
     */
    protected $ukey;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expireDate
     *
     * @param \DateTime $expireDate
     * @return Selectel
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;

        return $this;
    }

    /**
     * Get expireDate
     *
     * @return \DateTime 
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Selectel
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set uname
     *
     * @param string $uname
     * @return Selectel
     */
    public function setUname($uname)
    {
        $this->uname = $uname;

        return $this;
    }

    /**
     * Get uname
     *
     * @return string 
     */
    public function getUname()
    {
        return $this->uname;
    }

    /**
     * Set prefix
     *
     * @param string $prefix
     * @return Selectel
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get prefix
     *
     * @return string 
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Selectel
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set ukey
     *
     * @param string $ukey
     * @return Selectel
     */
    public function setUkey($ukey)
    {
        $this->ukey = $ukey;

        return $this;
    }

    /**
     * Get ukey
     *
     * @return string 
     */
    public function getUkey()
    {
        return $this->ukey;
    }
}
