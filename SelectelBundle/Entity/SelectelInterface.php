<?php

namespace itfind\SelectelBundle\Model;

interface SelectelInterface{

	/**
	 * Set expireDate
	 * @param \DateTime $expireDate
	 * @return self
	 */
	public function setExpireDate($expireDate);

	/**
	 * Get expireDate
	 * @return Object
	 */
	public function getExpireDate();

	/**
	 * Set token
	 * @param string $token
	 * @return self
	 */
	public function setToken($token);

	/**
	 * Get token
	 * @return string
	 */
	public function getToken();

	/**
	 * Set uname
	 * @param string $uname
	 * @return self
	 */
	public function setUname($uname);

	/**
	 * Get uname
	 *
	 * @return string
	 */
	public function getUname();

	/**
	 * Set prefix
	 *
	 * @param string $prefix
	 * @return self
	 */
	public function setPrefix($prefix);

	/**
	 * Get prefix
	 *
	 * @return string
	 */
	public function getPrefix();
	/**
	 * Set url
	 *
	 * @param string $url
	 * @return self
	 */
	public function setUrl($url);

	/**
	 * Get url
	 *
	 * @return string
	 */
	public function getUrl();

	/**
	 * Set ukey
	 *
	 * @param string $ukey
	 * @return self
	 */
	public function setUkey($ukey);

	/**
	 * Get ukey
	 *
	 * @return string
	 */
	public function getUkey();

}